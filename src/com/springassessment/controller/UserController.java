package com.springassessment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.springassessment.dao.UserDAO;
import com.springassessment.dao.impl.UserDAOImpl;
import com.springassessment.model.NewUser;
import com.springassessment.model.User;
import com.springassessment.service.UserService;
import com.springassessment.service.impl.UserServiceImpl;

@Controller
public class UserController {
	
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value="/")
	public String getHome()
	{
		
		
		return "main-menu";
	}

	@RequestMapping(value="/list")
	public String listUsers(Model theModel)
	{
		
		List<User> theUser= userService.getUsers();
		
		
		
		theModel.addAttribute("users",theUser);
		return "list-users";
	}
	
	@RequestMapping(value="/saveUser")
	public String saveUser(@ModelAttribute("user") User theUser)
	{
		userService.saveUser(theUser);
		return "redirect:/list";
		
		
	}
	@RequestMapping(value="/showFormForUpdate")
	public String updateUsers(@RequestParam("userid") int theId,Model theModel)
	{
		
		User theUser=userService.getUpatedUsers(theId);
		
		
		theModel.addAttribute("users",theUser);
		return "update-user";
	}
	@RequestMapping(value="/delete")
	public String deleteCustomer(@RequestParam("userid") int theId,Model theModel)
	{
		
		userService.deleteUser(theId);
		return "redirect:/list";
	}
	@RequestMapping(value="/showFormForUpdatePassword")
	public String changePassword(@RequestParam("userid") int theId,Model theModel)
	{
		
		User theUser=userService.changePassword(theId);
		
		
		
		theModel.addAttribute("users",theUser);
		
		return "change-password";
	}
}
