package com.springassessment.dao.impl;

import java.util.List;


import javax.sql.DataSource;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import com.springassessment.dao.UserDAO;
import com.springassessment.model.User;


@Repository
@Component
public class UserDAOImpl implements UserDAO {
	
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;
	
	

    /*static {
        try {
            // Build a SessionFactory object from session-factory config
            // defined in the hibernate.cfg.xml file. In this file we
            // register the JDBC connection information, connection pool,
            // the hibernate dialect that we used and the mapping to our
            // hbm.xml file for each pojo (plain old java object).
            Configuration config = new Configuration();
            sessionFactory = config.configure().addAnnotatedClass(User.class).buildSessionFactory();
        } catch (Throwable e) {
            System.err.println("Error in creating SessionFactory object."
                    + e.getMessage());
            throw new ExceptionInInitializerError(e);
        }
    }*/
	 
	
   
	
	 
	@Override

	public List<User> getUsers() {
		// TODO Auto-generated method stub
		System.out.println(sessionFactory);
		
		
		Session currentSession=sessionFactory.getCurrentSession();
		
		
		
		
		Query<User> theQuery=currentSession.createQuery("from User order by userId",User.class);
		
		List<User> users=theQuery.list();
		
		
		
		
		
		
		return users;
	}





	@Override
	public User getUpdatedUsers(int theId) {
		// TODO Auto-generated method stub
		Session currentSession=sessionFactory.getCurrentSession();
		
		
		User user=currentSession.get(User.class, theId);
		user.setFirstName(user.getFirstName());
		user.setMiddleName(user.getMiddleName());
		user.setLastName(user.getLastName());
		user.setEmail(user.getEmail());
		user.setMobile(user.getMobile());
		
		
		
		
		
		return user;
	}





	@Override
	public void saveUser(User theUser) {
		// TODO Auto-generated method stub
		Session currentSession=sessionFactory.getCurrentSession();
		
		System.out.println(theUser.getCreatedDate());
		currentSession.update(theUser);
		
		
	}





	@Override
	public void deleteUser(int theId) {
		// TODO Auto-generated method stub
		Session currentSession=sessionFactory.getCurrentSession();
		
		Query theQuery=currentSession.createQuery("delete from User where userId=:userid");
		
		theQuery.setParameter("userid", theId);
		
		theQuery.executeUpdate();
		
		
	}





	@Override
	public User changePassword(int theId) {
		// TODO Auto-generated method stub
		Session currentSession=sessionFactory.getCurrentSession();

		User user=currentSession.get(User.class, theId);
		
		user.setPassword(user.getPassword());
	
		
		
		
		return user;
		
	}



	
}
