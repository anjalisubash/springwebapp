package com.springassessment.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.springassessment.model.User;


public interface UserDAO {
	
	public List<User> getUsers();

	public User getUpdatedUsers(int theId);

	public void saveUser(User theUser);

	public void deleteUser(int theId);

	public User  changePassword(int theId);

}
