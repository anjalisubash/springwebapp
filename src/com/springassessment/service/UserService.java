package com.springassessment.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.springassessment.model.User;


public interface UserService {
	
	public List<User> getUsers();
	public User getUpatedUsers(int userId);

	public void saveUser(User theUser);
	public void deleteUser(int theId);
	public User changePassword(int theId);

}
