package com.springassessment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.springassessment.dao.UserDAO;
import com.springassessment.dao.impl.UserDAOImpl;
import com.springassessment.model.User;
import com.springassessment.service.UserService;

@Component
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserDAO userDAO;

	@Override
	@Transactional
	public User getUpatedUsers(int userId) {
		// TODO Auto-generated method stub
		return userDAO.getUpdatedUsers(userId);
	}

	@Override
	@Transactional
	public void saveUser(User theUser) {
		// TODO Auto-generated method stub
		userDAO.saveUser(theUser);
		
	}

	@Override
	@Transactional
	public List<User> getUsers() {
		// TODO Auto-generated method stub
		return userDAO.getUsers();
	}

	@Override
	@Transactional
	public void deleteUser(int theId) {
		// TODO Auto-generated method stub
		userDAO.deleteUser(theId);
		
	}

	@Override
	@Transactional
	public User changePassword(int theId) {
		// TODO Auto-generated method stub
		
		 return userDAO.changePassword(theId);
	}

}
