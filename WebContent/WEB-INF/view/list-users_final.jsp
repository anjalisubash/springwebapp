<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/Resources/CSS/style.css">

</head>
<body>



<!-- <div id="wrapper">
<div id="header">


	<h2>Users</h2>

</div>

</div> -->

<div id="container">
<div id="content">

<table>
	<tr>
		<th>User Id</th>
		<th>Name</th>
		<th>Email</th>
		<th>Mobile</th>
		<th>Date of Birth</th>
		<th>Action</th>
		<th></th>
		
	</tr>
	
	
	
	<c:forEach var="tempUser" items="${users}">
	<c:url var="updateLink" value="/showFormForUpdate">
		<c:param name="userid" value="${tempUser.userId}"/>
	</c:url>
	
	<c:url var="deleteLink" value="/delete">
		<c:param name="userid" value="${tempUser.userId}"/>
	</c:url>
	
	<c:url var="changePasswordLink" value="/showFormForUpdatePassword">
		<c:param name="userid" value="${tempUser.userId}"/>
		<c:param name="password" value="${tempUser.password}"/>
	</c:url>
		
	
	<tr>
		<td>${tempUser.userId}</td>
		<td>${tempUser.firstName} ${tempUser.lastName}</td>
		<td>${tempUser.email}</td>
		<td>${tempUser.mobile}</td>
		<td>${tempUser.dateOfBirth}</td>
		<td><a href="${updateLink}">edit</a>|
		<a href="${deleteLink}" onclick="if(!(confirm('Are you sure you want to delete this customer?'))) return false">delete</a>|
		<a href="${changePasswordLink}">Change password</a></td>
	
	</tr>
	
	
	
	
	</c:forEach>


</table>



</div>


</div>

</body>
</html>